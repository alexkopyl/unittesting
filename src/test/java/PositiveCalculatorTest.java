import model.Calculator;
import org.testng.Assert;
import org.testng.annotations.*;

public class PositiveCalculatorTest {
    @DataProvider
    public Object[][] positiveData() {
        return new Object[][] {
                {"+", "2", "3", 5},
                {"-", "-0.0", "-2.4", 2.4},
                {"*", "2", "4.8", 9.6},
                {"/", "2", "3", 0.667}
        };
    }

    @Test(dataProvider = "positiveData")
    public void positiveTest(String znak, String aStr, String bStr, double result) {
        Assert.assertEquals(Double.parseDouble(new Calculator().execute(new String[]{znak, aStr, bStr})), result);
    }
}
