import model.Calculator;
import model.CalculatorException;
import org.testng.annotations.*;

public class NegativeCalculatorTest {
    @DataProvider
    public Object[][] negativeData() {
        return new Object[][] {
                {"/", "1", "0"},
                {"/", "-1.1", "-0.0"},
                {"+", null, null},
                {"-", "one", "two"},
                {"plus", "1", "two"},
                {"+", "2147483647", "1"},
                {"+", "-2147483648", "-1"},
        };
    }

    @Test(dataProvider = "negativeData", expectedExceptions = {CalculatorException.class})
    public void negativeTest(String znak, String aStr, String bStr) {
        Calculator.execute(new String[]{znak, aStr, bStr});

    }
}
