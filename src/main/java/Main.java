import model.Calculator;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Calculator calculator = new Calculator();
        Scanner sc = new Scanner(System.in);

        String znak = sc.nextLine();
        String aStr = sc.nextLine();
        String bStr = sc.nextLine();

        System.out.println(calculator.execute(new String[]{znak, aStr, bStr}));
    }
}