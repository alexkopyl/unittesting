package model;

import sun.awt.SunToolkit;

import java.util.Locale;

public class Calculator {

    public static String execute(String[] params) {

        int resInt = 0;
        double resDouble = 0.0;

        String res = "";

            switch(params[0]) {
            case "+":
                try {
                if (params[1].equals(String.valueOf(Integer.MAX_VALUE)) || params[2].equals(String.valueOf(Integer.MAX_VALUE)) ||
                        params[1].equals(String.valueOf(Integer.MIN_VALUE)) || params[2].equals(Integer.MIN_VALUE)) {
                    throw new CalculatorException();
                }
                    int aInt = Integer.parseInt(params[1]);
                    int bInt = Integer.parseInt(params[2]);
                    resInt = aInt + bInt;
                    res = Integer.toString(resInt);
                    break;
                }catch (Exception e) {
                    throw new CalculatorException();
                }

            case "-":
                try {
                    double a1Double = Double.parseDouble(params[1]);
                    double b1Double = Double.parseDouble(params[2]);
                    resDouble = a1Double - b1Double;
                    res = Double.toString(resDouble);
                    break;
                }catch (Exception e) {
                    throw new CalculatorException();
                }

            case "*":
                try {
                    double a2Double = Double.parseDouble(params[1]);
                    double b2Double = Double.parseDouble(params[2]);
                    resDouble = a2Double * b2Double;
                    res = Double.toString(resDouble);
                    break;
                }
                catch (Exception e) {
                    throw new CalculatorException();
                }

            case "/":
                if (params[2].equals("0") || params[2].equals("-0") || params[2].equals("0.0") || params[2].equals("-0.0")) {
                    throw new CalculatorException();
                }
                try{
                    double a3Double = Double.parseDouble(params[1]);
                    double b3Double = Double.parseDouble(params[2]);
                    resDouble = a3Double / b3Double;
                    res = String.format(Locale.ROOT, "%.3f", resDouble);
                    break;
                }
                catch (Exception e) {
                    throw new CalculatorException();
                }
                default:
                    throw new CalculatorException();
        }
        return res;
    }
}
